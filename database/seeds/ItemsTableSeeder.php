<?php

use App\Item;
use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::create([
            'user_id'    => 1,
            'vendor_sku' => 'TGS1',
            'sku'        => 'TGS1',
            'title'      => 'TGS1'
        ])->cartons()->create([
            'user_id'          => 1,
            'items_per_carton' => 10,
            'length'           => 0.28,
            'width'            => 0.26,
            'height'           => 0.34,
            'gross_weight'     => 34.2,
            'cbm'              => 0.025
        ]);

        Item::create([
            'user_id'    => 1,
            'vendor_sku' => 'TGS2',
            'sku'        => 'TGS2',
            'title'      => 'TGS2'
        ])->cartons()->create([
            'user_id'          => 1,
            'items_per_carton' => 10,
            'length'           => 0.28,
            'width'            => 0.26,
            'height'           => 0.34,
            'gross_weight'     => 34.2,
            'cbm'              => 0.025
        ]);

        Item::create([
            'user_id'    => 1,
            'vendor_sku' => 'TGS3',
            'sku'        => 'TGS3',
            'title'      => 'TGS3'
        ])->cartons()->create([
            'user_id'          => 1,
            'items_per_carton' => 10,
            'length'           => 0.28,
            'width'            => 0.26,
            'height'           => 0.34,
            'gross_weight'     => 34.2,
            'cbm'              => 0.025
        ]);

        Item::create([
            'user_id'    => 1,
            'vendor_sku' => 'TGS4',
            'sku'        => 'TGS4',
            'title'      => 'TGS4'
        ])->cartons()->create([
            'user_id'          => 1,
            'items_per_carton' => 10,
            'length'           => 0.28,
            'width'            => 0.26,
            'height'           => 0.34,
            'gross_weight'     => 34.2,
            'cbm'              => 0.025
        ]);

        Item::create([
            'user_id'    => 1,
            'vendor_sku' => 'TGS5',
            'sku'        => 'TGS5',
            'title'      => 'TGS5'
        ])->cartons()->create([
            'user_id'          => 1,
            'items_per_carton' => 10,
            'length'           => 0.28,
            'width'            => 0.26,
            'height'           => 0.34,
            'gross_weight'     => 34.2,
            'cbm'              => 0.025
        ]);

        Item::create([
            'user_id'    => 1,
            'vendor_sku' => 'TGS6',
            'sku'        => 'TGS6',
            'title'      => 'TGS6'
        ])->cartons()->create([
            'user_id'          => 1,
            'items_per_carton' => 10,
            'length'           => 0.28,
            'width'            => 0.26,
            'height'           => 0.34,
            'gross_weight'     => 34.2,
            'cbm'              => 0.025
        ]);

        Item::create([
            'user_id'    => 1,
            'vendor_sku' => 'TGS7',
            'sku'        => 'TGS7',
            'title'      => 'TGS7'
        ])->cartons()->create([
            'user_id'          => 1,
            'items_per_carton' => 10,
            'length'           => 0.28,
            'width'            => 0.26,
            'height'           => 0.34,
            'gross_weight'     => 34.2,
            'cbm'              => 0.025
        ]);

        Item::create([
            'user_id'    => 1,
            'vendor_sku' => 'TGS8',
            'sku'        => 'TGS8',
            'title'      => 'TGS8'
        ])->cartons()->create([
            'user_id'          => 1,
            'items_per_carton' => 10,
            'length'           => 0.28,
            'width'            => 0.26,
            'height'           => 0.34,
            'gross_weight'     => 34.2,
            'cbm'              => 0.025
        ]);

        Item::create([
            'user_id'    => 1,
            'vendor_sku' => 'TGS9',
            'sku'        => 'TGS9',
            'title'      => 'TGS9'
        ])->cartons()->create([
            'user_id'          => 1,
            'items_per_carton' => 10,
            'length'           => 0.28,
            'width'            => 0.26,
            'height'           => 0.34,
            'gross_weight'     => 34.2,
            'cbm'              => 0.025
        ]);
    }
}
