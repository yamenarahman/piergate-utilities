<?php

use App\Container;
use Illuminate\Database\Seeder;

class ContainersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Container::create([
            'name'     => '20\' standard-steel',
            'payload'  => 28300,
            'capacity' => 33
        ]);

        Container::create([
            'name'     => '40\' standard-steel',
            'payload'  => 28800,
            'capacity' => 67
        ]);

        Container::create([
            'name'     => '40\' high-steel',
            'payload'  => 28690,
            'capacity' => 76
        ]);

        Container::create([
            'name'     => '45\' high-steel',
            'payload'  => 27650,
            'capacity' => 85
        ]);
    }
}
