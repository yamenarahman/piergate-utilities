@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">
                <i class="fa fa-refresh" aria-hidden="true"></i>
                <a href="/synnex-po">Synnex Purchase Order</a>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <form class="form-horizontal my-5" method="POST" action="{{ route('synnex.po') }}">
                @csrf
                <div class="form-group">
                  <label for="order">Order</label>
                  <input type="text" class="form-control" name="order" id="order" aria-describedby="helpId" placeholder="">
                  <small id="helpId" class="form-text text-muted">Help text</small>
                </div>

                <button type="submit" class="btn btn-primary btn-lg btn-block">Purchase</button>
            </form>
        </div>
    </div>
</div>
@endsection
