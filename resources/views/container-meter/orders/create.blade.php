@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">
                    <i class="fa fa-thermometer" aria-hidden="true"></i>
                    <a href="/container-meter">Container Meter</a>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h3>Create new order</h3>
            </div>
        </div>
        <order mode="create"></order>
    </div>
@endsection