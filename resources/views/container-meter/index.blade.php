@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">
                <i class="fa fa-thermometer" aria-hidden="true"></i>
                <a href="/container-meter">Container Meter</a>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                        <i class="fa fa-home"></i> Home
                    </a>
                    <a class="nav-item nav-link" id="nav-containers-tab" data-toggle="tab" href="#nav-containers" role="tab" aria-controls="nav-containers" aria-selected="false">
                        <i class="fa fa-dropbox"></i> Containers
                    </a>
                    <a class="nav-item nav-link" id="nav-items-tab" data-toggle="tab" href="#nav-items" role="tab" aria-controls="nav-items" aria-selected="false">
                        <i class="fa fa-tag"></i> Items
                    </a>
                    <a class="nav-order nav-link" id="nav-orders-tab" data-toggle="tab" href="#nav-orders" role="tab" aria-controls="nav-orders" aria-selected="false">
                        <i class="fa fa-cart-plus"></i> Orders
                    </a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <h3>General reports</h3>
                </div>
                <div class="tab-pane fade" id="nav-containers" role="tabpanel" aria-labelledby="nav-containers-tab">
                    <containers></containers>
                </div>
                <div class="tab-pane fade" id="nav-items" role="tabpanel" aria-labelledby="nav-items-tab">
                    <items></items>
                </div>
                <div class="tab-pane fade" id="nav-orders" role="tabpanel" aria-labelledby="nav-orders-tab">
                    <orders></orders>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
