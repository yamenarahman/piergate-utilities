@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">
                <i class="fa fa-refresh" aria-hidden="true"></i>
                <a href="/synnex-sync">Synnex Instant Sync</a>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            @if ($item != false)
                <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $item['description'] }}</h4>
                    <p class="card-text d-flex flex-column">
                        <h5 class="align-self-center">{{ $item['synnexSKU'] }}</h5>
                        <span class="align-self-center">Price: {{ $item['price'] }}</span>
                        <span class="align-self-center">Quantity: {{ $item['totalQuantity'] }}</span>
                    </p>
                </div>
                </div>
            @else
                <h4>Not found.</h4>
            @endif
        </div>
    </div>
</div>
@endsection
