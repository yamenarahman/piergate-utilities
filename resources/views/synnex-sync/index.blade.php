@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">
                <i class="fa fa-refresh" aria-hidden="true"></i>
                <a href="/synnex-sync">Synnex Instant Sync</a>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <form class="form-inline my-5" method="POST" action="{{ route('sync') }}">
                @csrf
                <div class="d-flex flex-column">
                    <div class="form-group">
                        <label for="sku">SKU: </label>
                        <input type="text" name="sku" id="sku" class="form-control ml-1" placeholder="sku" aria-describedby="sku-help">
                    </div>

                    <button class="btn btn-primary mt-1">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
