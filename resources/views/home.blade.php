@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="card-deck col-12">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title text-center"><i class="fa fa-thermometer" aria-hidden="true"></i> Container Meter</h3>
                    <p class="card-text"><a href="/container-meter" class="btn btn-primary btn-block">Manage</a></p>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title text-center"><i class="fa fa-tv" aria-hidden="true"></i> TV parts</h3>
                    <p class="card-text"><a href="/tv-parts" class="btn btn-primary btn-block">Manage</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center mt-3">
        <div class="card-deck col-12">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title text-center"><i class="fa fa-refresh" aria-hidden="true"></i> Synnex Instant Sync</h3>
                    <p class="card-text"><a href="/synnex-sync" class="btn btn-primary btn-block">Manage</a></p>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title text-center"><i class="fa fa-cart-plus" aria-hidden="true"></i> Synnex Purchase Order</h3>
                    <p class="card-text"><a href="/synnex-po" class="btn btn-primary btn-block">Manage</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection