<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/container-meter', 'HomeController@containerMeter');
Route::get('/synnex-sync', 'HomeController@synnexSync');
Route::post('/synnex-sync/sync', 'SynnexSyncController@sync')->name('sync');
Route::get('/synnex-po', 'HomeController@synnexPO');
Route::post('/synnex-po/orders', 'SynnexPOController@store')->name('synnex.po');
Route::get('/tv-parts', 'HomeController@tvParts');
Route::resource('/container-meter/containers', 'ContainersController')->except(['create', 'edit']);
Route::resource('/container-meter/items', 'ItemsController')->except(['create', 'edit']);
Route::resource('/container-meter/orders', 'OrdersController');
Route::get('/download', 'SynnexPOController@download');
