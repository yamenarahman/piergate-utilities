<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carton extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
