<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cartons()
    {
        return $this->hasMany(Carton::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'orders_items');
    }
}
