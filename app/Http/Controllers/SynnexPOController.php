<?php

namespace App\Http\Controllers;

use App\Synnex\ApiClient;
use Illuminate\Http\Request;

class SynnexPOController extends Controller
{
    public function store(Request $request)
    {
        // return $request->all();
        $service = new ApiClient();

        return $service->purchaseOrder();
    }

    public function download()
    {
        $service = new ApiClient();

        return $service->downloadFile();
    }
}
