<?php

namespace App\Http\Controllers;

use App\Synnex\ApiClient;
use Illuminate\Http\Request;

class SynnexSyncController extends Controller
{
    public function Sync()
    {
        $service = new ApiClient();

        return $item = $service->getAvailability(request('sku'));

        return view('synnex-sync.result', compact('item'));
    }
}
