<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['items' => Item::with('user', 'cartons')->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'vendor_sku'      => 'required|string|unique:items',
            'sku'             => 'required|string|unique:items',
            'title'           => 'required|string',
            'description'     => 'nullable',
            'qty'             => 'nullable|numeric',
            'cost'            => 'nullable|numeric',
            'additional_cost' => 'nullable|numeric',
            'length'          => 'nullable|numeric',
            'width'           => 'nullable|numeric',
            'height'          => 'nullable|numeric',
            'weight'          => 'nullable|numeric',
            'volume'          => 'nullable|numeric',
            'itemsPerCarton'  => 'required|numeric',
            'cartonLength'    => 'required|numeric',
            'cartonWidth'     => 'required|numeric',
            'cartonHeight'    => 'required|numeric',
            'grossWeight'     => 'required|numeric',
            'cbm'             => 'required|numeric'
        ]);

        $item = auth()->user()->items()->create($request->only([
            'vendor_sku',
            'sku',
            'title',
            'description',
            'qty',
            'cost',
            'additional_cost',
            'length',
            'width',
            'height',
            'weight',
            'volume'
        ]));

        $item->cartons()->create([
            'user_id'          => auth()->id(),
            'items_per_carton' => $request->itemsPerCarton,
            'length'           => $request->cartonLength,
            'width'            => $request->cartonWidth,
            'height'           => $request->cartonHeight,
            'gross_weight'     => $request->grossWeight,
            'cbm'              => $request->cbm,
            'weight'           => $request->grossWeight - ($item->weight * $request->itemsPerCarton)
        ]);

        if ($item) {
            return response()->json([
                'item'    => $item->load('user', 'cartons'),
                'type'    => 'success',
                'message' => 'Item <b>' . $item->title . '</b> is added successfully!'
            ]);
        }

        return response()->json([
            'type'    => 'error',
            'message' => 'Something went wrong, Try again later.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->delete();

        return response()->json([
            'type'      => 'success',
            'message'   => 'Container <b>' . $item->title . '</b> is deleted successfully!'
        ]);
    }
}
