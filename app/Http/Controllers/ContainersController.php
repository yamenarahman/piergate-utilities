<?php

namespace App\Http\Controllers;

use App\Container;
use Illuminate\Http\Request;

class ContainersController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['containers' => Container::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'     => 'required|string|max:255',
            'payload'  => 'required|numeric|min:1',
            'capacity' => 'required|numeric|min:1'
        ]);

        $container = Container::create($request->only(['name', 'payload', 'capacity']));

        if ($container) {
            return response()->json([
                'container' => $container,
                'type'      => 'success',
                'message'   => 'Container <b>' . $container->name . '</b> is added successfully!'
            ]);
        }

        return response()->json([
            'type'    => 'error',
            'message' => 'Something went wrong, Try again later.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Container $container)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Container $container)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Container $container)
    {
        $container->delete();

        return response()->json([
            'type'      => 'success',
            'message'   => 'Container <b>' . $container->name . '</b> is deleted successfully!'
        ]);
    }
}
