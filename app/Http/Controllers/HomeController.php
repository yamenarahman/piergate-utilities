<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function containerMeter()
    {
        return view('container-meter.index');
    }

    public function synnexSync()
    {
        return view('synnex-sync.index');
    }

    public function synnexPO()
    {
        return view('synnex-po.index');
    }

    public function tvParts()
    {
        return redirect('/home');
    }
}
