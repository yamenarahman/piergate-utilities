<?php

namespace App\Synnex;

use Storage;
use SimpleXMLElement;
use GuzzleHttp\Client;

class ApiClient
{
    protected $config;
    protected $client;

    public function __construct($config = [])
    {
        if (empty($config)) {
            $config = [
                'account'  => env('SYNNEX_ACCOUNT'),
                'user'     => env('SYNNEX_USER'),
                'password' => env('SYNNEX_PASSWORD')
            ];
        }

        $this->config = $config;

        $this->client = new Client([
            'base_uri' => 'https://ec.synnex.com/SynnexXML/'
            // 'base_uri' => 'https://testec.synnex.com/SynnexXML/'
        ]);
    }

    public function getAvailability($sku)
    {
        $xmlRequest = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><priceRequest></priceRequest>');

        $xmlRequest->addChild('customerNo', $this->config['account']);
        $xmlRequest->addChild('userName', $this->config['user']);
        $xmlRequest->addChild('password', $this->config['password']);

        $skulist = $xmlRequest->addChild('skuList');

        $skulist->addChild('synnexSKU', htmlspecialchars($sku));
        $skulist->addChild('lineNumber', 1);

        $xmlRequest = $xmlRequest->asXML();

        $response = $this->client->request('POST', 'PriceAvailability', [
            'body' => $xmlRequest
        ]);

        return $item = json_decode(json_encode(new SimpleXMLElement($response->getBody())), true);

        if (isset($item['errorMessage'])) {
            return false;
        }

        // ‘Active’, ‘Discontinued’, ‘Not found’
        switch ($item['PriceAvailabilityList']['status']) {
            case 'Active':
                \Log::info($item['PriceAvailabilityList']);
                return $item['PriceAvailabilityList'];
                break;
            case 'Discontinued':
            case 'Not Authorized':
            case 'Not found':
                return false;
                break;
            default:
                return false;
                break;
        }
    }

    public function purchaseOrder($order = null)
    {
        $xmlRequest = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><SynnexB2B></SynnexB2B>');

        $credentials = $xmlRequest->addChild('Credential');

        $credentials->addChild('UserID', $this->config['account']);
        $credentials->addChild('Password', $this->config['password']);

        $orderRequest = $xmlRequest->addChild('OrderRequest');
        $orderRequest->addChild('CustomerNumber', $this->config['account']);
        $orderRequest->addChild('PONumber', '123');
        $orderRequest->addChild('DropShipFlag', 'N'); // Y, N

        $shipment = $orderRequest->addChild('Shipment');
        $shipment->addChild('ShipFromWarehouse', 2); // warehouse number

        $shipTo = $shipment->addChild('ShipTo');
        $shipTo->addChild('AddressName1', 'address');
        $shipTo->addChild('AddressLine1', 'address');
        $shipTo->addChild('City', 'city');
        $shipTo->addChild('State', 'state');
        $shipTo->addChild('ZipCode', 12612);
        $shipTo->addChild('Country', 'US'); // CA

        $contact = $shipment->addChild('ShipToContact');
        $contact->addChild('ContactName', 'name');
        $contact->addChild('PhoneNumber', '123-456');
        $contact->addChild('EmailAddress', 'example@email.com');

        $method = $shipment->addChild('ShipMethod');
        $method->addChild('Code', 'FX'); // fedex

        $payment = $orderRequest->addChild('Payment');
        $code = $payment->addChild('BillTo');
        $code->addAttribute('code', $this->config['account']);

        $endUserPO = $orderRequest->addChild('EndUserPONumber', 1234);

        $comment = $orderRequest->addChild('Comment', 'Thanks');

        $items = $orderRequest->addChild('Items');
        // foreach ($variable as $key => $value) {
        $item = $items->addChild('Item');
        $item->addAttribute('lineNumber', 1); // iterator
        $item->addChild('SKU', '12345');
        $item->addChild('UnitPrice', 7.45);
        $item->addChild('OrderQuantity', 5);
        // }

        $xmlRequest = $xmlRequest->asXML();

        $response = $this->client->request('POST', 'PO', [
            'body' => $xmlRequest
        ]);

        return $result = json_decode(json_encode(new SimpleXMLElement($response->getBody())), true);
    }

    public function setSynnexConfig($envKey, $envValue)
    {
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);

        $oldValue = env($envKey);

        $str = str_replace("{$envKey}={$oldValue}", "{$envKey}={$envValue}", $str);

        $fp = fopen($envFile, 'w');

        fwrite($fp, $str);
        fclose($fp);
    }

    public function downloadFile()
    {
        $ftp = Storage::createFtpDriver([
            'host' => 'ftp.synnex.com',
            'username' => 'u563384',
            'password' => 'E8b3g7K5',
            'port' => '21', // your ftp port
            'timeout' => '3600', // timeout setting
        ]);

        $downloaded = $ftp->put(storage_path('app/public'), $ftp->get('563384.zip'));

        return $downloaded;
    }
}
